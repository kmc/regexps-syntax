The KMC Regular Expression Parser
===
A centralized implementation of a regular expression parser (parsing the
regular expressions themselves, *not* RE parsing on input strings).

Instead of having a bunch of different ad-hoc implementations, we can use this one as an off-the-shelf parser.

(BTW: See Text.Regex.PCRE.QQ)
